cd (dirname (status -f))/..

[ ! -d "$DESTDIR" ] && echo "No such directory: $DESTDIR"
[ ! -d "$DESTDIR" ] && exit

mkdir -p $DESTDIR/usr $DESTDIR/usr/share $DESTDIR/usr/share/xsessions
cp xsessions/unityx.desktop $DESTDIR/usr/share/xsessions

mkdir -p $DESTDIR/usr/share/unityx
cp -r ./*/ $DESTDIR/usr/share/unityx && \
  rm -r $DESTDIR/usr/share/unityx/xsessions
cp unityx $DESTDIR/usr/share/unityx
