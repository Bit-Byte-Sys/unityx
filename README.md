# UnityX

## About

UnityX is the successor of Unity7. It brings forward Unity7's keyboard-centric features and is aimed at normal computers and small tablets with keyboards/palmtops.

## Dependencies

UnityX's dependencies can be found in DEBBUILD file.

## Building

* Install `qckdeb` from https://gitlab.com/unity-x/qckdeb.
* Execute `qckdeb` in this folder.
