source config/config.fish

function settings_daemon --description 'Settings daemon'
    # Get configuration
    set val get_val org.unityx.settings-daemon
    set wm ($val wm)
    set applaunch ($val applaunch)
    set panel ($val panel)
    set desktop_icons ($val desktop_icons)

    # Set GTK theme and background
    export GTK_THEME=($val gtk_theme)
    feh --bg-fill ($val background) &
    
    # Assign keybindings to rofi.
    python3 keybindings/keybindings.py '<Alt>w' 'rofi -show window' '' &
    python3 keybindings/keybindings.py '<Super>a' 'rofi -show drun' '' &
    python3 keybindings/keybindings.py '<Alt>a' 'rofi -show drun' '' &

    # Assign keybinding to tint2.
    python3 keybindings/keybindings.py '<Alt>d' 'tint2' '' &

    # Add remaining keybindings.
    fish keybindings/add.fish &

    # Execute the essential components
    mkdir -p ~/Desktop
    fish -c "cd ~/Desktop && $desktop_icons" &
    $wm &

    # Execute autorun script, if it exists.
    fish .config/unityx/autorun || true

    # Assign keybinding to log out.
    python3 keybindings/keybindings.py '<Alt>x' 'exit' 'exit'

    # Kill all background processes launched
    kill (jobs -p)
end
