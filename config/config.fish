function get_val --description 'Get value of a key.'
    # Source OS config
    source "/usr/share/unityx/config/$argv[1].cfg" &>/dev/null || true

    # Source system-wide config
    source "/etc/unityx/config/$argv[1].cfg" &>/dev/null || true

    # Source user config
    source "$HOME/.config/unityx/$argv[1].cfg" &>/dev/null || true

    # Source build config
    source "build_config/$argv[1].cfg" &>/dev/null

    # Get value
    set val (string split ' ' -- $$argv[2])

    # Print the value of the key to stdout
    echo "$val"
end